﻿module FFT
implicit none    

contains
    
recursive subroutine BitReversalSorting(X,Y,Wn) !по сути это bit reversal sorting algorithm
complex, dimension(0:), intent(in) :: X
complex, dimension(0:size(X)-1), intent(out) :: Y
complex, dimension(0:), intent(in) :: Wn
integer(4) :: N, i

    N=size(X)

    if (N>2) then
        call BitReversalSorting(X(0:N/2-1)+X(N/2:N-1),Y(0:N-2:2),Wn(0:N-1:2))
        call BitReversalSorting(Wn(0:N/2-1)*(X(0:N/2-1)-X(N/2:N-1)),Y(1:N-1:2),Wn(0:N-1:2))
    else
        Y(0)=X(0)+X(1)
        Y(1)=X(0)-X(1)
    endif
end subroutine BitReversalSorting  
    

pure function expMinus(q,p) result(W) !на этом месте компилятор начал писать код за меня
integer(4), intent(in) :: q, p
complex(4) :: W

    W=exp(-2.0*(4*atan(1.0))*cmplx(0,1)*q/p)
end function expMinus


pure function expPlus(q,p) result(W)
integer(4), intent(in) :: q, p
complex(4) :: W

    W=exp(2.0*(4*atan(1.0))*cmplx(0,1)*q/p)
end function expPlus
    
end module FFT   